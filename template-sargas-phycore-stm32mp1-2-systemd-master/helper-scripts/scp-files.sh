#!/bin/bash

DEPLOY="sargas-phycore-stm32mp1-2-systemd-master/tmp/deploy/images/phycore-stm32mp1-2"

pushd /workdir/build/${DEPLOY}

SOURCE_1="FlashLayout_sdcard_phycore-stm32mp1-2-trusted.raw"

TARGET_1="student@192.168.42.60:/home/student/projects/${DEPLOY}"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
