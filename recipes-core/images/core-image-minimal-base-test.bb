# Copyright (C) 2021 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-core/images/core-image-minimal-base.bb

IMAGE_INSTALL += "\
                  telegraf \
                 "

#IMAGE_INSTALL += "\
#                  conserver \
#                 "

#IMAGE_INSTALL += "\
#                  screen \
#                  byobu \
#                 "

#IMAGE_INSTALL += "\
#                  usbutils \
#                 "

IMAGE_FEATURES += "package-management"
